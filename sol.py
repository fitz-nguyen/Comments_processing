import numpy as np
import pandas as pd

#Train data
data_train = pd.read_csv('/home/kaz/MachineLearning/Titanic/train.csv')

data_train = data_train.replace(["male","female"],[0,1])
data_train = data_train.replace(["S","C","Q"],[0,1,2])
data_train = data_train.fillna(0)

X = data_train[["PassengerId","Pclass","Sex","Age","SibSp","Parch","Fare","Embarked"]]
y = data_train[["Survived"]]
X = X.astype(np.float32).values
y = y.astype(np.float32).values

#Test data
data_test = pd.read_csv('/home/kaz/MachineLearning/Titanic/test.csv')

data_test = data_test.replace(["male","female"],[0,1])
data_test = data_test.replace(["S","C","Q"],[0,1,2])
data_test = data_test.fillna(0)

X_Test = data_test[["PassengerId","Pclass","Sex","Age","SibSp","Parch","Fare","Embarked"]]
X_Test = X_Test.astype(np.float32).values

from sklearn.cross_validation import train_test_split
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, BatchNormalization, Activation
from keras.wrappers.scikit_learn import KerasRegressor

seed = 42
np.random.seed(seed)

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.2)

#NN
model = Sequential()
model.add(Dense(8, input_shape=(8,)))
model.add(BatchNormalization())
model.add(Activation("relu"))
model.add(Dropout(0.4))

model.add(Dense(8))
model.add(BatchNormalization())
model.add(Activation("sigmoid"))
model.add(Dropout(0.4))
    
model.add(Dense(4))
model.add(BatchNormalization())
model.add(Activation("sigmoid"))
model.add(Dropout(0.4))
    
model.add(Dense(2, activation="sigmoid"))
    
# output layer
model.add(Dense(1, activation='linear'))

#Compile
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

#Train
model.fit(X, y, nb_epoch=300, batch_size=30)

#Predict
predictions = np.round(model.predict(X_Test))

predictions = pd.DataFrame(predictions)
predictions.columns = ['Survived']

result = pd.concat([data_test[["PassengerId"]],predictions], axis = 1)

result.to_csv("result.csv", index=False)

