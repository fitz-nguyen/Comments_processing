from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB,BernoulliNB
from nltk.tokenize import word_tokenize
import nltk
import random
import json
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
import csv
from nltk.corpus import stopwords
# import numpy

documents = []
words = []

def results(model, name_csv):
    testing_set = []
    with open('test.json', encoding = 'utf_8') as file:
        store = json.loads(file.read())
        for data in store:
            testing_set.append((word_tokenize(data["text"]), data["id"]))
        del store
        file.close()
    #print(MNB_classifier.classify(find_features(testing_set[0][0]))
    with open(name_csv, 'w', newline='') as csvfile:
        fieldnames = ['id', 'sentiment']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for test in testing_set:
        	result = model.classify(find_features(test[0]))
        	writer.writerow({'id': test[1], 'sentiment': result})


with open('train.json', encoding = 'utf_8') as f:
    datastore = json.loads(f.read())
    for data in datastore:
        documents.append((word_tokenize(data["text"]), data["sentiment"]))
        words.extend(word_tokenize(data["text"]))
    del datastore
    f.close()
# words.flatten()
print(len(documents))
#print(len(documents))
#documents = documents[:4700]
random.shuffle(documents)

all_words = []
stop_words = set(stopwords.words('russian'))
for w in words:
    all_words.append(w.lower())
del words
filtered_sentence = [w for w in all_words if not w in stop_words]
del all_words
filtered_sentence = nltk.FreqDist(filtered_sentence)
word_features = list(filtered_sentence.keys())[:25000]
del filtered_sentence


def find_features(document):
    word = set(document)
    features = {}
    for w in word_features:
        features[w] = (w in word)
    return features


# print((find_features(movie_reviews.words('neg/cv000_29416.txt'))))
featuresets = [(find_features(rev), category) for (rev, category) in documents]
print(featuresets)

# set that we'll train our classifier with
training_set = featuresets[:8000]
del documents
# set that we'll test against.
testing_set = featuresets[8000:]
# classifier = nltk.NaiveBayesClassifier.train(training_set)
# print("Classifier accuracy percent:", (nltk.classify.accuracy(classifier, testing_set)) * 100)

# del classifier
# MNB_classifier = SklearnClassifier(MultinomialNB())
# MNB_classifier.train(training_set)
# results(MNB_classifier, 'results_MNB.csv')
# print("MultinomialNB accuracy percent:", nltk.classify.accuracy(MNB_classifier, testing_set))
# del MNB_classifier
# BNB_classifier = SklearnClassifier(BernoulliNB())
# BNB_classifier.train(training_set)
# print("BernoulliNB accuracy percent:", nltk.classify.accuracy(BNB_classifier, testing_set))
# results(BNB_classifier, 'results_BNB.csv')
# del BNB_classifier

# LogisticRegression_classifier = SklearnClassifier(LogisticRegression())
# LogisticRegression_classifier.train(training_set)
# print("LogisticRegression_classifier accuracy percent:", (nltk.classify.accuracy(LogisticRegression_classifier, testing_set))*100)
# results(LogisticRegression_classifier, 'results_logistic.csv')
# del LogisticRegression_classifier

SGDClassifier_classifier = SklearnClassifier(SGDClassifier())
SGDClassifier_classifier.train(training_set)
print("SGDClassifier_classifier accuracy percent:", (nltk.classify.accuracy(SGDClassifier_classifier, testing_set))*100)
results(SGDClassifier_classifier, 'results_SGDC.csv')
# del SGDClassifier_classifier
# SVC_classifier = SklearnClassifier(SVC())
# SVC_classifier.train(training_set)
# print("SVC_classifier accuracy percent:", (nltk.classify.accuracy(SVC_classifier, testing_set))*100)
# results(SVC_classifier, 'results_SVC.csv')
# del SVC_classifier
# LinearSVC_classifier = SklearnClassifier(LinearSVC())
# LinearSVC_classifier.train(training_set)
# print("LinearSVC_classifier accuracy percent:", (nltk.classify.accuracy(LinearSVC_classifier, testing_set))*100)
# results(LinearSVC_classifier, 'results_linear.csv')
# del LinearSVC_classifier
# NuSVC_classifier = SklearnClassifier(NuSVC())
# NuSVC_classifier.train(training_set)
# print("NuSVC_classifier accuracy percent:", (nltk.classify.accuracy(NuSVC_classifier, testing_set))*100)
# results(NuSVC_classifier, 'results_NuSVC.csv')
# del NuSVC_classifier
